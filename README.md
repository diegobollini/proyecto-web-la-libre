# Proyecto Web La Libre

Proyecto de web para La Libre desarrollado en el curso de [Desarrollo web en CoderHouse](https://gitlab.com/diegobollini/curso-desarrollo-web).  
Originalmente el sitio fue un Wordpress pero no tenía sentido tanta infraestructura. Además mejor hacerlo desde cero y usarlo como proyecto final (certificado) para el curso 🤓.

## Sobre el Proyecto

- Estoy usando Visual Studio Code Version: 1.54.2 (SO: elementary OS 5.1.7 Hera)
- Extensiones VSC: HTML Snippets, indent-rainbow, Live Sass Compiler, Prettier, Visual Studio IntelliCode
- No utilicé templates de páginas, pero sí bastante [Bootstrap](https://getbootstrap.com/) (navbar, carousel, cards, columns)
- Intenté dejar comentarios / links de todos los bloques, códigos, estilos que use en el sitio

## GitLab Pages

La configuración fue muy pero muy rápido con [esta guía](https://docs.gitlab.com/ee/user/project/pages/index.html) y los templates que ya existen en GitLab. Una vez que el repositorio está actualizado con los respectivos html, scss y demás, se crea el archivo .gitlab-ci.yml donde se define en qué carpeta están los archivos y sobre qué rama se hace el deploy.  
Esta es la versión [test](https://diegobollini.gitlab.io/proyecto-web-la-libre/) de la web desde el repositorio. Eventualmente estará alojada en [lalibrecf.com.ar](https://lalibrecf.com.ar).

## Recursos y contenidos del curso

### Estructura HTML

- <https://www.w3schools.com/html/default.asp>
- <https://developer.mozilla.org/es/docs/Web/HTML>
- <http://html5doctor.com/tag/html-5/>
- <https://www.diegocmartin.com/estructura-basica-de-una-pagina-html/>/>
- <https://disenowebakus.net/aprender-html-lenguaje-de-marcado-de-hypertexto.php>
- <https://www.hostingatope.com/como-hacer-una-pagina-web-con-html/>
- <https://www.lawebera.es/diseno-web/estructura-de-una-pagina-web-estructura-del-diseno.php>
- <https://www.mclibre.org/consultar/htmlcss/html/html-formularios.html>

### CSS

- <http://www.usabilidad.tv/hojas_de_estilos_css/css.asp>
- <https://www.eniun.com/que-es-css-cascading-style-sheets/>
- [Diferencia entre Class y ID](https://devcode.la/tutoriales/diferencias-class-id/)
- [Tipografias](https://fonts.google.com/)
- [Unidades de Medida CSS](https://lenguajecss.com/css/modelo-de-cajas/unidades-css/)
- [Colores CSS](https://lenguajecss.com/css/colores-y-fondos/colores-css/)

#### Paletas de colores

- <https://colorhunt.co/palettes/popular>
- <https://www.materialpalette.com/>
- <https://color.adobe.com/>
- <https://coolors.co/>
- [Colores de marcas especificas](https://brandcolors.net/)

#### Fondos CSS

- <https://lenguajecss.com/css/colores-y-fondos/fondos-css/>
- <https://developer.mozilla.org/es/docs/Web/CSS/background>
- <https://www.mclibre.org/consultar/htmlcss/css/css-fondos.html>

#### Varios CSS

- [Unidades de Medidas](https://lenguajecss.com/css/modelo-de-cajas/unidades-css/)
- [Margin / Padding](https://lenguajecss.com/css/modelo-de-cajas/margenes-y-rellenos/)
- [Overflow](https://www.eniun.com/propiedad-overflow-excedente-contenido-css/)
- <https://cybmeta.com/em-y-rem>
- <https://www.programandoamedianoche.com/2018/02/como-funcionan-em-y-rem/>
- <https://www.programandoamedianoche.com/2018/01/como-funcionan-las-unidades-de-medida-vh-vw-vmin-y-vmax/>

### Modelo de Cajas

- <https://css-tricks.com/the-css-box-model/>
- <https://developer.mozilla.org/es/docs/Web/CSS/CSS_Modelo_Caja/Introducci%C3%B3n_al_modelo_de_caja_de_CSS>
- <https://lenguajecss.com/css/modelo-de-cajas/que-es/>
- <https://www.eniun.com/modelo-cajas-css-margenes-relleno-bordes/>

### Flexbox

- <http://www.falconmasters.com/css/guia-completa-flexbox/>
- <https://developer.mozilla.org/es/docs/Web/CSS/CSS_Flexible_Box_Layout/Usando_las_cajas_flexibles_CSS>
- <https://css-tricks.com/snippets/css/a-guide-to-flexbox/>
- <https://lenguajecss.com/css/maquetacion-y-colocacion/flexbox/>
- <https://www.eniun.com/flexbox-modelo-caja-flexible-css/>
- <https://ed.team/blog/guia-definitiva-de-flexbox-2-flex-basis-flex-frow-flex-shrink>
- [Juego Flexbox](https://flexboxfroggy.com/#es)
- [Otro juego Flexbox](http://www.flexboxdefense.com/)

### GRIDS

- <https://lenguajecss.com/css/maquetacion-y-colocacion/grid-css/>
- <https://css-tricks.com/snippets/css/complete-guide-grid/>
- <https://gridbyexample.com/examples/>
- <https://www.w3schools.com/css/css_grid.asp>
- <https://escss.blogspot.com/2015/12/guia-css-grid-layout.html>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/gap>
- <https://www.adictosaltrabajo.com/2018/01/30/maquetacion-con-css-grid/>
- [Juego Grids](https://cssgridgarden.com/#es)
- <https://css-tricks.com/snippets/css/complete-guide-grid/>
- <https://www.w3schools.com/css/css_grid.asp>
- <https://lenguajecss.com/css/maquetacion-y-colocacion/grid-css/>
- <https://cssgridgarden.com/#es>
- <https://grid.layoutit.com/>

### Transiciones, Transformaciones y Animaciones

- <https://css-tricks.com/almanac/properties/t/transform/>
- <https://xitrus.es/blog/87/Efecto_flip_con_fotos_en_3D_con_CSS3>
- <https://www.enriquejros.com/efectos-imagenes-css/>
- <https://aprende-web.net/NT/anim/anim_5.php>
- <https://lenguajecss.com/css/animaciones/transiciones/>
- <http://w3.unpocodetodo.info/css3/transitions.php>
- <http://www.falconmasters.com/css/transiciones-css3/>
- <https://codingpotions.com/animaciones-css>
- <https://desarrolloweb.com/articulos/intro-animaciones-css.html>
- <https://developer.mozilla.org/es/docs/Web/CSS/CSS_Animations/Usando_animaciones_CSS>
- <https://creatuweb.espaciolatino.com/tutorcss3/transf3D.html>
- <https://www.eniun.com/transformaciones-css-rotar-torcer-escalar-desplazar/>
- <https://www.w3schools.com/css/css3_3dtransforms.asp>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/rotateZ>

### SASS

- <https://sass-lang.com/>
- <https://desarrolloweb.com/articulos/que-es-sass-usar-sass.html>
- <https://uniwebsidad.com/libros/sass>
- <https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/tutorial-de-sass/>
- <https://www.sassmeister.com/>
- <https://prepros.io/>

### BEM

- <https://blog.interactius.com/metodolog%C3%ADa-css-block-element-modifier-bem-f26e69d1de3>
- <https://blog.ida.cl/desarrollo/metodologia-bem-desarrollo-front-end/#:~:text=La%20metodolog%C3%ADa%20BEM%20divide%20la,de%20desarrollo%20basada%20en%20componentes>

### SEO

- <https://web.dev/>
- <https://developers.google.com/speed/pagespeed/insights/>
- <https://web.dev/vitals/>
- <https://www.inboundemotion.com/blog/cuantas-palabras-clave-se-recomiendan-en-seo>
- <https://search.google.com/test/mobile-friendly?hl=es>
- [Title](<https://www.albertofdez.com/meta-title-html-seo/>)
- [Meta Description](<https://es.semrush.com/blog/como-hacer-una-meta-descripcion-perfecta/>)

#### Keywords

- <https://rockcontent.com/es/blog/keywords/>
- <https://keywordtool.io/es>
- <https://neilpatel.com/ubersuggest/>
- <https://ads.google.com/aw/keywordplanner/home?sf=kp&subid=ar-es-419-et-g-aw-a-tools-kwp_bb-awhp_xin1!o2>
- <https://trends.google.com/trends/?hl=es&geo=MX>

### Optimización de imágenes

- <https://www.40defiebre.com/optimizar-imagenes-web>
- <https://kinsta.com/es/blog/optimizar-imagenes-para-la-web/>
- <https://compressjpeg.com/es/>
- <https://tinyjpg.com/>
- <https://www.befunky.com/create/resize-image/>
